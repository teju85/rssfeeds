import json
import urllib2
import re
import string

categoriesJsonFile = 'categories.json'


def evaluateCategories(name, cat=""):
    categories = json.load(open(categoriesJsonFile, 'r'))
    if cat == "":
        first = True
    else:
        first = False
    for regex in categories:
        category = categories[regex]
        if cat.find(category) >= 0:
            continue
        reg = re.compile(regex, re.IGNORECASE)
        if not reg.findall(name):
            continue
        if first:
            cat = category
            first = False
            continue
        cat = cat + ',' + category
    return cat

def prepareFeed(url, name, cat=""):
    feed = {"url" : url,
            "name" : name,
            "categories" : evaluateCategories(name, cat)}
    return feed

def printStatus(feedName):
    print("Obtaining feeds from '%s'..." % feedName)

def readHtml(url, removeNewLines=False, removeComments=False, hdr=None):
    if hdr != None:
        req = urllib2.Request(url, headers=hdr)
    else:
        req = urllib2.Request(url)
    conn = urllib2.urlopen(req)
    content = conn.read()
    if removeNewLines:
        content = string.replace(content, '\n', '')
        content = string.replace(content, '\r', '')
        content = string.replace(content, '\t', '')
    if removeComments:
        content = re.sub(r'<!--.*?-->', '', content)
    return content

def getMatches(content, regexStr):
    if type(regexStr) is str:
        urlNameFinder = re.compile(regexStr)
        urls = re.findall(urlNameFinder, content)
        return urls
    urls = []
    for reg in regexStr:
        urlNameFinder = re.compile(reg)
        urls.extend(re.findall(urlNameFinder, content))
    return urls

def obtainFeedsFor(feedName, feedUrl, regexStr, defaultCategories="", urlIndex=0,
                   nameIndex=1, removeNewLines=False, urlPrefix="", removeComments=False,
                   urlSuffix="", feedGenerator=None, hdr=None):
    printStatus(feedName)
    content = readHtml(feedUrl, removeNewLines, removeComments, hdr)
    urls = getMatches(content, regexStr)
    feeds = []
    for u in urls:
        uurl = u[urlIndex]
        uname = u[nameIndex]
        if feedGenerator != None:
            (uname, uurl) = feedGenerator(uname, uurl)
        if uname == "":
            continue
        uurl = urlPrefix + uurl + urlSuffix
        feeds.append(prepareFeed(uurl, feedName+' - '+uname, defaultCategories))
    return feeds
