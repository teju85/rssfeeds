#!/usr/bin/env python

import json
import importlib
import feeds
import types



def getFeedsAutomatically():
    allFeeds = []
    for item in dir(feeds):
        item = feeds.__dict__.get(item)
        if isinstance(item, types.FunctionType):
            allFeeds.extend(item())
    return allFeeds

def getAllFeeds():
    manualJsonFile = 'manual.json'
    print("Obtaining manually added feeds...")
    manual = json.load(open(manualJsonFile, 'r'))
    auto = getFeedsAutomatically()
    print("Total of %d manual feeds" % len(manual))
    print("Total of %d automatic feeds" % len(auto))
    allFeeds = []
    allFeeds.extend(manual)
    allFeeds.extend(auto)
    allFeeds = sorted(allFeeds, key=lambda feed: feed['name']);
    return allFeeds

def getAllCategories(feeds):
    print("Collecting all categories...")
    catMap = {}
    for f in feeds:
        cats = f['categories'].split(',')
        for c in cats:
            catMap[c] = 1
    categories = catMap.keys()
    categories.append('')
    categories = sorted(categories)
    return categories

def outputJson(data):
    outputJsonFile = 'feeds.json'
    print("Writing feeds and categories into %s..." % outputJsonFile)
    json.dump(data, open(outputJsonFile, 'w'), indent=2, separators=(',',': '))

def main():
    allFeeds = getAllFeeds()
    allCategories = getAllCategories(allFeeds)
    outputJson({"feeds" : allFeeds, "categories" : allCategories})

if __name__ == '__main__':
    main()
