import common
import re


def feedsDilbert():
    reg = '<a href="(http://feed.dilbert.com.*?)" target="_blank">\s*?'
    reg = reg + '<img src=".*?" alt="(.*?)"'
    return common.obtainFeedsFor(feedName='Dilbert',
                                 feedUrl='http://dilbert.com/rss/',
                                 regexStr=reg,
                                 defaultCategories='Comic')

def feedsTOI():
    reg = '<tr><td width="\d+"><a href="([^"]*?.cms)" id="\S+?">(.*?)</a></td>'
    return common.obtainFeedsFor(feedName='Times Of India',
                                 feedUrl='http://timesofindia.indiatimes.com/rss.cms',
                                 regexStr=reg,
                                 defaultCategories='News,Entertainment')

def feedsIBN():
    return common.obtainFeedsFor(feedName='CNN IBN',
                                 feedUrl='http://ibnlive.in.com/rss/',
                                 regexStr='<td><a href="(.*?)"><b>(.*?)</b></a></td>',
                                 defaultCategories='News,Entertainment')

def feedsHindu():
    return common.obtainFeedsFor(feedName='Hindu',
                                 feedUrl='http://www.thehindu.com/navigation/?type=rss',
                                 regexStr='href="(.*?service=rss)".*>\s*(.*?)</a>',
                                 defaultCategories='News,Entertainment')

def feedsGizmodo():
    reg = []
    reg.append('<a href="(/rss_section_feeds.*?)".*?</i>(.*?)</a>')
    reg.append('<a href="(/rss_tag_section_feeds.*?)".*?</i>(.*?)</a>')
    url = 'http://www.lifehacker.co.in/rss_feeds.cms'
    pref = 'http://www.lifehacker.co.in'
    def skipWork(nam, ur):
        if nam == 'Work' and  'rss_section_feeds' in ur:
            return ("", "")
        return (nam, ur)
    feeds = []
    feeds.extend(common.obtainFeedsFor(feedName='Gizmodo',
                                       feedUrl=url,
                                       regexStr=reg,
                                       defaultCategories='General',
                                       urlPrefix=pref,
                                       feedGenerator=skipWork))
    feeds.extend(common.obtainFeedsFor(feedName='Lifehacker',
                                       feedUrl=url,
                                       regexStr=reg,
                                       defaultCategories='General',
                                       urlPrefix=pref,
                                       feedGenerator=skipWork))
    return feeds

def feedsEconomicTimes():
    reg = '<tr><td width="\d+"><a href="([^"]*?.cms)" id="\S+">(.*?)</a></td>'
    return common.obtainFeedsFor(feedName='Economic Times',
                                 feedUrl='http://economictimes.indiatimes.com/rss.cms',
                                 regexStr=reg,
                                 defaultCategories='Business,Economics,Finance')

def feedsTheHoot():
    reg = 'class="synopsis">(.*?)</td><td><a href="../(.*?)">'
    return common.obtainFeedsFor(feedName='The Hoot',
                                 feedUrl='http://www.thehoot.org/web/home/hootrss.php',
                                 regexStr=reg,
                                 defaultCategories='News',
                                 urlIndex=1,
                                 nameIndex=0,
                                 removeNewLines=True,
                                 urlPrefix='http://www.thehoot.org/web/')

def feedsIndianExpress():
    reg = '<td>(.*?)</td><td><a href="(.*?)" target="_blank">'
    return common.obtainFeedsFor(feedName='Indian Express',
                                 feedUrl='http://indianexpress.com/syndication/',
                                 regexStr=reg,
                                 defaultCategories='News',
                                 urlIndex=1,
                                 nameIndex=0,
                                 removeNewLines=True)

def feedsTechRadar():
    reg = '<a href="(/rss.*?)" title=".*?">(.*?)</a>'
    def nameFilter(nam, ur):
        nam = nam.replace(' feeds', '')
        nam = nam.replace(' feed', '')
        if ur.find('/rss/reviews') >= 0  and  nam.find('review') < 0:
            nam = nam + ' Reviews'
        return (nam, ur)
    return common.obtainFeedsFor(feedName='TechRadar',
                                 feedUrl='http://www.techradar.com/rsstoolkit',
                                 regexStr=reg,
                                 defaultCategories='Tech',
                                 urlPrefix='http://www.techradar.com',
                                 urlSuffix='?format=xml',
                                 feedGenerator=nameFilter)

def feedsTechCrunch():
    reg = '<h5><a target="_blank" href="(.*?)">(.*?)</a></h5>'
    def nameFilter(nam, ur):
        nam = nam.replace(' feed', '')
        nam = re.sub(r'^TechCrunch', '', nam)
        if nam == "":
            nam = 'All'
        return (nam, ur)
    return common.obtainFeedsFor(feedName='TechCrunch',
                                 feedUrl='http://techcrunch.com/rssfeeds/',
                                 regexStr=reg,
                                 defaultCategories='Tech',
                                 urlSuffix='?format=xml',
                                 feedGenerator=nameFilter)

def feedsWired():
    reg = '<a href="(.+?)".*?><img .*? alt="View Feed" .*?> (.*?)</a>'
    def nameFilter(nam, ur):
        nam = re.sub(r'^\s*Wired(.com)\s*', '', nam)
        nam = re.sub(r'^\s*Wired\s*', '', nam)
        return (nam, ur)
    return common.obtainFeedsFor(feedName='Wired',
                                 feedUrl='http://www.wired.com/about/rss_feeds/',
                                 removeNewLines=True,
                                 regexStr=reg,
                                 defaultCategories='Tech',
                                 feedGenerator=nameFilter)

def feedsNYTimes():
    regs = []
    r1 = '<div class="rssSection">\s*<a href="(.*?)">\s*<span class="rssRow">(.*?)</span>'
    r2 = '<ul class="rssColumns">\s*<li><a href="(.*?)">(.*?)</a>'
    r3 = '<ul class="rssSubsection">\s*<li\s*><a href="(.*?)">(.*?)</a>'
    r4 = '<li\s*class="last"><a href="(.*?)">(.*?)</a>'
    regs.append(r1)
    regs.append(r2)
    regs.append(r3)
    regs.append(r4)
    url = 'http://www.nytimes.com/services/xml/rss/index.html'
    def nameFilter(nam, ur):
        nam = re.sub(r'^\s*NYTimes.com\s*', '', nam)
        return (nam, ur)
    return common.obtainFeedsFor(feedName='NYTimes',
                                 feedUrl=url,
                                 removeNewLines=True,
                                 regexStr=regs,
                                 defaultCategories='News',
                                 feedGenerator=nameFilter)

def feedsNseIndia():
    reg = '<link href="(.*?)" title="(.*?)" type="application/rss\+xml"'
    def nameFilter(nam, ur):
        nam = re.sub(r'^\s*NSE - \s*', '', nam)
        nam = re.sub(r'\s*\[RSS\]', '', nam)
        return (nam, ur)
    hdrs = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
    return common.obtainFeedsFor(feedName='NSE India',
                                 feedUrl='http://www.nseindia.com/',
                                 regexStr=reg,
                                 hdr=hdrs,
                                 defaultCategories='Stocks,Finance',
                                 feedGenerator=nameFilter,
                                 urlSuffix='?format=xml')

def feedsMoneyControl():
    reg = '<a href="(.*?)" target="_new" class="bl_13" title="(.*?)">(.*?)</a>'
    url = 'http://www.moneycontrol.com/india/newsarticle/rssfeeds/rssfeeds.php'
    return common.obtainFeedsFor(feedName='Money Control',
                                 feedUrl=url,
                                 regexStr=reg,
                                 defaultCategories='Finance',
                                 urlSuffix='?format=xml')

def feedsCNET():
    reg = '<li><a href="(http://www.cnet.com/rss/.*?)" target="_blank">(.*?)</a></li>'
    def nameFilter(nam, ur):
        if 'Podcasts' in nam:
            return ("", "")
        nam = re.sub(r'^\s*CNET\s*', '', nam)
        return (nam, ur)
    return common.obtainFeedsFor(feedName='CNET',
                                 feedUrl='http://www.cnet.com/rss/',
                                 regexStr=reg,
                                 defaultCategories='Tech,News',
                                 feedGenerator=nameFilter)

def feedsRottenTomatoes():
    regs = []
    regs.append('<strong>\s*?<a href="(/syndication/rss/.*?)">(.*?)</a>\s*?</strong>')
    regs.append('<a href="(/syndication/rss/[^"]*?)">\s*?<strong>([^<>]*?)</strong>\s*?</a>')
    url = 'http://www.rottentomatoes.com/help_desk/syndication_rss/'
    def nameFilter(nam, ur):
        nam = re.sub(r'\s+', ' ', nam)
        if 'complete_dvds.xml' in ur:
            nam = 'Complete DVDs'
        return (nam, ur)
    hdrs = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
    return common.obtainFeedsFor(feedName='RottenTomatoes',
                                 feedUrl=url,
                                 regexStr=regs,
                                 hdr=hdrs,
                                 removeNewLines=True,
                                 defaultCategories='Entertainment',
                                 urlPrefix='http://www.rottentomatoes.com',
                                 feedGenerator=nameFilter)

def feedsTelegraph():
    regs = []
    r1 = '<div class="summarySmall rssFeed">.*?<h3><a href="(.*?)">(.*?)</a></h3?'
    r2 = '<div class="summary summaryMedium rssFeed">.*?<h3><a href="(.*?)">(.*?)</a></h3?'
    r3 = '<div class="summary summarySmall rssFeed">.*?<h3><a href="(.*?)">(.*?)</a></h3?'
    regs.append(r1)
    regs.append(r2)
    regs.append(r3)
    foundUrls = {}
    def nameFilter(nam, ur):
        if ur in foundUrls:
            return ("", "")
        foundUrls[ur] = True
        return (nam, ur)
    return common.obtainFeedsFor(feedName='Telegraph',
                                 feedUrl='http://www.telegraph.co.uk/rssfeeds/',
                                 regexStr=regs,
                                 removeNewLines=True,
                                 defaultCategories='News',
                                 feedGenerator=nameFilter)

def feedsDrDobbs():
    reg = '<link href="(.*?)".*?type="application/rss\+xml" title="(.*?)"'
    def nameFilter(nam, ur):
        nam = re.sub(r'Dr. Dobb\'s ', '', nam)
        return (nam, ur)
    return common.obtainFeedsFor(feedName='Dr Dobbs',
                                 feedUrl='http://www.drdobbs.com/rss/',
                                 regexStr=reg,
                                 urlPrefix='https://www.drdobbs.com',
                                 defaultCategories='Tech',
                                 feedGenerator=nameFilter)

def feedsReuters():
    reg = '<td class="xmlLink"><a href="(.*?)">(.*?)</a></td>';
    return common.obtainFeedsFor(feedName='Reuters',
                                 feedUrl='http://www.reuters.com/tools/rss',
                                 regexStr=reg,
                                 defaultCategories='News')

def feedsPopularMechanics():
    regs = []
    r1 = '</p><p><strong>(.+?)</strong></p><p>Copy and paste this link into your reader:</p><p><a href="(http://www.popularmechanics.co.za/.+?)" target="_blank">[^<]*?</a></p>'
    r2 = '</p><p><strong>(.+?)</strong></p><p>Copy and paste this link into your reader:</p><p>(http://www.popularmechanics.co.za/.+?)"</p>'
    regs.append(r1)
    regs.append(r2)
    def nameFilter(nam, ur):
        nam = re.sub(r'\xc2\xa0', ' ', nam)
        return (nam, ur)
    return common.obtainFeedsFor(feedName='Popular Mechanics',
                                 feedUrl='http://www.popularmechanics.co.za/rss-feeds/',
                                 urlIndex=1,
                                 nameIndex=0,
                                 regexStr=regs,
                                 removeNewLines=True,
                                 defaultCategories='Automotive',
                                 feedGenerator=nameFilter)

def feedsDigit():
    reg = '<li><a href="(.+?)" target="_blank">(.+?)</a></li>'
    def nameFilter(nam, ur):
        nam = re.sub(r'^Digit ', '', nam)
        if nam == "":
            nam = 'All'
        return (nam, ur)
    return common.obtainFeedsFor(feedName='Digit',
                                 feedUrl='http://www.digit.in/rss-feed/',
                                 regexStr=reg,
                                 defaultCategories='Tech',
                                 feedGenerator=nameFilter,
                                 removeComments=True,
                                 removeNewLines=True)
