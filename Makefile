
default:
	@echo "make what? Available targets are:"
	@echo "  push   - push changes to bitbucket master branch"
	@echo "  commit - create feeds and commit them locally"
	@echo "  feeds  - create the feeds list"
	@echo "  diff   - diff the current and previous json files"
	@echo "  clean  - clean up the temporary files"

push:
	git push -u origin master

commit: feeds diff
	$(MAKE) clean
	git add .
	git commit

feeds:
	./collector.py
	gzip -f feeds.json

diff:
	gunzip -c feeds.json.gz > tmp.json
	tkdiff previous.json tmp.json &
	@sleep 2
	@echo "In case the diff is bad, press Ctrl-C to stop submission now..."
	@read ff
	cp tmp.json previous.json

clean:
	rm -f feeds.json *.pyc tmp.json
